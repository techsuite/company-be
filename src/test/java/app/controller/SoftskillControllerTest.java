package app.controller;

import app.dao.SoftskillDAO;
import app.model.Softskill;
import app.model.request.cuestionario.section.CuestionarioRequestSoftskill;
import com.google.gson.Gson;
import one.util.streamex.EntryStream;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(SoftskillController.class)
public class SoftskillControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private SoftskillDAO softskillDAO;

  @Test
  public void getSoftskills_whenCalledWithNoSoftskills_shouldReturn204StatusCode()
    throws Exception {
    List<Softskill> empty = List.of();
    when(softskillDAO.getSoftskills()).thenReturn(empty);

    mockMvc.perform(get("/softskill")).andExpect(status().isNoContent());
  }

  @Test
  public void getSoftskills_whenCalledWhenThereAreSoftskills_shouldRetrieveThem()
    throws Exception {
    List<Softskill> dummySoftskillsList = List.of(
      new Softskill(10, "Liderazgo"),
      new Softskill(20, "Aprender por mi cuenta")
    );
    when(softskillDAO.getSoftskills()).thenReturn(dummySoftskillsList);

    String outputBody = mockMvc
      .perform(get("/softskill"))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$[*]", hasSize(dummySoftskillsList.size())))
      .andReturn()
      .getResponse()
      .getContentAsString();

    EntryStream
      .of(new Gson().fromJson(outputBody, Softskill[].class))
      .forKeyValue(
        (index, output) ->
          assertThat(output).isEqualTo(dummySoftskillsList.get(index))
      );
  }

  @Test
  public void getOpcionesRendimiento_whenCalled_shouldRetrieveThem()
    throws Exception {
    var expectedResult = CuestionarioRequestSoftskill.Rendimiento.values();
    String outputBody = mockMvc
      .perform(get("/softskill/opciones-rendimiento"))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$[*]", hasSize(expectedResult.length)))
      .andReturn()
      .getResponse()
      .getContentAsString();

    EntryStream
      .of(
        new Gson()
        .fromJson(outputBody, CuestionarioRequestSoftskill.Rendimiento[].class)
      )
      .forKeyValue(
        (index, output) -> assertThat(output).isEqualTo(expectedResult[index])
      );
  }
}
