package app.controller;

import app.dao.EmpleadoDAO;
import app.model.Empleado;
import com.google.gson.Gson;
import one.util.streamex.EntryStream;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static app.model.Empleado.Categoria;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(EmpleadoController.class)
public class EmpleadoControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private EmpleadoDAO empleadoDAO;

  @Test
  public void getEmpleados_whenCallingWithNonExistingEmployees_shouldReturn204StatusCode()
    throws Exception {
    when(empleadoDAO.getEmpleados()).thenReturn(List.of());

    mockMvc.perform(get("/team")).andExpect(status().isNoContent());
  }

  @Test
  public void getEmpleados_whenCallingWithExistingEmployees_shouldReturnThemCorrectly()
    throws Exception {
    List<Empleado> dummyEmployees = List.of(
      new Empleado("z17m012", "Pedro Gonzalez", null, Categoria.developers),
      new Empleado(
        "z17m013",
        "Paula Torres",
        "https://mypicture.com/paula",
        Categoria.executives
      )
    );
    when(empleadoDAO.getEmpleados()).thenReturn(dummyEmployees);

    String outputBody = mockMvc
      .perform(get("/team"))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$[*]", hasSize(dummyEmployees.size())))
      .andReturn()
      .getResponse()
      .getContentAsString();

    EntryStream
      .of(new Gson().fromJson(outputBody, Empleado[].class))
      .forKeyValue(
        (index, output) ->
          assertThat(output).isEqualTo(dummyEmployees.get(index))
      );
  }

  @Test
  public void getEmpleado_whenCallingWithNonExistingEmployee_shouldReturn204StatusCode()
    throws Exception {
    when(empleadoDAO.getEmpleado(anyString())).thenReturn(null);
    when(empleadoDAO.getEmpleado2(anyString())).thenReturn(null);

    mockMvc.perform(get("/team/notExisting")).andExpect(status().isNoContent());
  }

  @Test
  public void getEmpleado_whenCallingWithExistingEmployee_shouldReturnItWithOkStatusCode()
    throws Exception {
    Empleado empleado = new Empleado(
      "z17m012",
      "Dummy",
      "https://dummy.com",
      Categoria.developers
    );
    when(empleadoDAO.getEmpleado(anyString())).thenReturn(empleado);
    when(empleadoDAO.getEmpleado2(anyString())).thenReturn(empleado);

    mockMvc
      .perform(get("/team/z17m012"))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$[*]", hasSize(4)))
      .andExpect(jsonPath("$.matricula", is(empleado.getMatricula())))
      .andExpect(jsonPath("$.nombre", is(empleado.getNombre())))
      .andExpect(jsonPath("$.foto_url", is(empleado.getFoto_url())));
  }
}
