package app.controller;

import app.dao.TecnologiaDAO;
import app.model.Tecnologia;
import app.model.request.cuestionario.section.CuestionarioRequestTecnologia;
import com.google.gson.Gson;
import one.util.streamex.EntryStream;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(TecnologiaController.class)
public class TecnologiaControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private TecnologiaDAO tecnologiaDAO;

  @Test
  public void getTecnologias_whenCalledWithNoTechnologies_shouldReturn204StatusCode()
    throws Exception {
    List<Tecnologia> empty = List.of();
    when(tecnologiaDAO.getTecnologias()).thenReturn(empty);

    mockMvc.perform(get("/tecnologia")).andExpect(status().isNoContent());
  }

  @Test
  public void getTecnologias_whenCalledWhenThereAreTechnologies_shouldRetrieveThem()
    throws Exception {
    List<Tecnologia> dummyTechList = List.of(
      new Tecnologia(1, "Java"),
      new Tecnologia(2, "Angular"),
      new Tecnologia(3, "Spring Boot")
    );
    when(tecnologiaDAO.getTecnologias()).thenReturn(dummyTechList);

    String outputBody = mockMvc
      .perform(get("/tecnologia"))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$[*]", hasSize(dummyTechList.size())))
      .andReturn()
      .getResponse()
      .getContentAsString();

    EntryStream
      .of(new Gson().fromJson(outputBody, Tecnologia[].class))
      .forKeyValue(
        (index, output) ->
          assertThat(output).isEqualTo(dummyTechList.get(index))
      );
  }

  @Test
  public void getOpcionesConocimiento_whenCalled_shouldRetrieveOptions()
    throws Exception {
    var expectedResult = CuestionarioRequestTecnologia.GradoConocimiento.values();
    String outputBody = mockMvc
      .perform(get("/tecnologia/opciones-conocimiento"))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$[*]", hasSize(expectedResult.length)))
      .andReturn()
      .getResponse()
      .getContentAsString();

    EntryStream
      .of(
        new Gson()
        .fromJson(
            outputBody,
            CuestionarioRequestTecnologia.GradoConocimiento[].class
          )
      )
      .forKeyValue(
        (index, output) -> assertThat(output).isEqualTo(expectedResult[index])
      );
  }
}
