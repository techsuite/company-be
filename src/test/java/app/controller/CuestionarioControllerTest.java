package app.controller;

import app.model.request.cuestionario.CuestionarioRequest;
import app.service.CuestionarioService;
import mocks.CuestionarioMocks;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CuestionarioController.class)
public class CuestionarioControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private CuestionarioService cuestionarioService;

  @Test
  public void nuevoCuestionario_whenCalled_shouldCallCuestionarioServiceWithCorrectData()
    throws Exception {
    CuestionarioRequest request = CuestionarioMocks.filledRequest();

    mockMvc
      .perform(
        post("/cuestionario")
          .content(new JSONObject(request).toString())
          .contentType(MediaType.APPLICATION_JSON)
      )
      .andExpect(status().isOk())
      .andExpect(content().string(""));

    var captor = ArgumentCaptor.forClass(CuestionarioRequest.class);
    verify(cuestionarioService).newCuestionario(captor.capture());

    CuestionarioRequest passedRequest = captor.getValue();
    assertThat(passedRequest).isEqualTo(request);
  }
}
