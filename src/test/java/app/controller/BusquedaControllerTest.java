package app.controller;

import app.dao.BusquedaDAO;
import app.model.Busqueda;
import com.google.gson.Gson;
import one.util.streamex.EntryStream;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(BusquedaController.class)
public class BusquedaControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private BusquedaDAO busquedaDAO;

  @Test
  public void getBusquedas_whenThereAreNoBusquedas_shouldReturnNoContentStatusCode()
    throws Exception {
    List<Busqueda> empty = List.of();
    when(busquedaDAO.getBusquedas()).thenReturn(empty);
    when(busquedaDAO.getBusquedas2()).thenReturn(empty);

    mockMvc.perform(get("/search")).andExpect(status().isNoContent());
  }

  @Test
  public void getBusquedas_whenThereAreSomePresent_shouldReturnThemCorrectly()
    throws Exception {
    List<Busqueda> dummySearches = List.of(
      new Busqueda(1, "dummy1", 100L),
      new Busqueda(2, "dummy2", 200L),
      new Busqueda(2, "dummy3", 300L)
    );
    when(busquedaDAO.getBusquedas()).thenReturn(dummySearches);
    when(busquedaDAO.getBusquedas2()).thenReturn(dummySearches);

    String outputBody = mockMvc
      .perform(get("/search"))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$[*]", hasSize(dummySearches.size())))
      .andReturn()
      .getResponse()
      .getContentAsString();

    EntryStream
      .of(new Gson().fromJson(outputBody, Busqueda[].class))
      .forKeyValue(
        (index, output) ->
          assertThat(output).isEqualTo(dummySearches.get(index))
      );
  }
}
