package app.controller;

import app.dao.CargoDAO;
import app.model.Cargo;
import app.model.request.cuestionario.section.CuestionarioRequestCargo;
import com.google.gson.Gson;
import one.util.streamex.EntryStream;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CargoController.class)
public class CargoControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private CargoDAO cargoDAO;

  @Test
  public void getCargos_whenCalledWithNoCargos_shouldReturn204StatusCode()
    throws Exception {
    List<Cargo> empty = List.of();
    when(cargoDAO.getCargos()).thenReturn(empty);

    mockMvc.perform(get("/cargo")).andExpect(status().isNoContent());
  }

  @Test
  public void getCargos_whenCalledWhenThereAreCargos_shouldRetrieveThem()
    throws Exception {
    List<Cargo> dummyCargosList = List.of(
      new Cargo(4, "Front End"),
      new Cargo(5, "Back End"),
      new Cargo(6, "Tester")
    );
    when(cargoDAO.getCargos()).thenReturn(dummyCargosList);

    String outputBody = mockMvc
      .perform(get("/cargo"))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$[*]", hasSize(dummyCargosList.size())))
      .andReturn()
      .getResponse()
      .getContentAsString();

    EntryStream
      .of(new Gson().fromJson(outputBody, Cargo[].class))
      .forKeyValue(
        (index, output) ->
          assertThat(output).isEqualTo(dummyCargosList.get(index))
      );
  }

  @Test
  public void getOpcionesPreferencia_whenCalled_shouldRetrieveThem()
    throws Exception {
    var expectedResult = CuestionarioRequestCargo.Preferencia.values();
    String outputBody = mockMvc
      .perform(get("/cargo/opciones-preferencia"))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$[*]", hasSize(expectedResult.length)))
      .andReturn()
      .getResponse()
      .getContentAsString();

    EntryStream
      .of(
        new Gson()
        .fromJson(outputBody, CuestionarioRequestCargo.Preferencia[].class)
      )
      .forKeyValue(
        (index, output) -> assertThat(output).isEqualTo(expectedResult[index])
      );
  }
}
