package app.service;

import app.dao.CuestionarioDAO;
import app.dao.EmpleadoDAO;
import app.model.Cuestionario;
import app.model.Empleado;
import app.model.request.cuestionario.CuestionarioRequest;
import mocks.CuestionarioMocks;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static app.model.Empleado.Categoria;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CuestionarioServiceTest {

  @Autowired
  private CuestionarioService cuestionarioService;

  @MockBean
  private EmpleadoService empleadoService;

  @MockBean
  private EmpleadoDAO empleadoDAO;

  @MockBean
  private CuestionarioDAO cuestionarioDAO;

  private CuestionarioRequest request;

  @Before
  public void setUp() {
    this.request = CuestionarioMocks.filledRequest();
  }

  @Test
  public void newCuestionario_whenEmpleadoDoesNotExist_shouldCallServiceToCreateIt()
    throws Exception {
    when(empleadoService.exists(anyString())).thenReturn(false);

    cuestionarioService.newCuestionario(request);

    var captor = ArgumentCaptor.forClass(Empleado.class);
    verify(empleadoDAO).insertEmpleado(captor.capture());
    Empleado passedEmpleado = captor.getValue();

    assertThat(passedEmpleado)
      .isEqualTo(
        new Empleado(
          request.getMatricula(),
          request.getNombre(),
          null,
          Categoria.developers
        )
      );
  }

  @Test
  public void newCuestionario_whenEmpleadoDoesExists_shouldNotCallServiceToCreateIt()
    throws Exception {
    when(empleadoService.exists(anyString())).thenReturn(true);

    cuestionarioService.newCuestionario(request);

    verifyNoInteractions(empleadoDAO);
  }

  @Test
  public void newCuestionario_whenCuestionarioDoesNotExists_shouldCallServiceToCreateIt()
    throws Exception {
    when(cuestionarioDAO.getCuestionario(anyString())).thenReturn(null);

    cuestionarioService.newCuestionario(request);

    var captor = ArgumentCaptor.forClass(Cuestionario.class);
    verify(cuestionarioDAO).insertCuestionario(captor.capture());
    Cuestionario passedCuestionario = captor.getValue();

    assertThat(passedCuestionario)
      .isEqualTo(
        new Cuestionario(
          null,
          request.getComentariosTecnologias(),
          request.getComentariosSoftskills(),
          request.getComentariosCargo(),
          request.getMatricula()
        )
      );
  }

  @Test
  public void newCuestionario_whenCuestionarioDoesExist_shouldNotCallServiceToCreateItAndClearExistingRelationsAndUpdateComments()
    throws Exception {
    Cuestionario mockCuestionario = new Cuestionario(
      14,
      "uno",
      "dos",
      "tres",
      "z17m051"
    );
    when(cuestionarioDAO.getCuestionario(anyString()))
      .thenReturn(mockCuestionario);

    cuestionarioService.newCuestionario(request);

    verify(cuestionarioDAO, times(0)).insertCuestionario(any());
    verify(cuestionarioDAO).clearExistingRelations(14);
    verify(cuestionarioDAO).updateCuestionario(mockCuestionario);
  }

  @Test
  public void newCuestionario_whenCalled_shouldInsertRelatedInfo()
    throws Exception {
    // case Cuestionario does not exist
    when(cuestionarioDAO.getCuestionario(anyString())).thenReturn(null);
    when(cuestionarioDAO.insertCuestionario(any())).thenReturn(23);

    cuestionarioService.newCuestionario(request);

    verify(cuestionarioDAO)
      .insertRelatedTechnologies(23, request.getTecnologias());
    verify(cuestionarioDAO)
      .insertRelatedSoftskills(23, request.getSoftskills());
    verify(cuestionarioDAO).insertRelatedCargos(23, request.getCargos());

    // case Cuestionario does exist
    when(cuestionarioDAO.getCuestionario(anyString()))
      .thenReturn(new Cuestionario(88, "uno", "dos", "tres", "z17m051"));

    cuestionarioService.newCuestionario(request);

    verify(cuestionarioDAO)
      .insertRelatedTechnologies(88, request.getTecnologias());
    verify(cuestionarioDAO)
      .insertRelatedSoftskills(88, request.getSoftskills());
    verify(cuestionarioDAO).insertRelatedCargos(88, request.getCargos());
  }
}
