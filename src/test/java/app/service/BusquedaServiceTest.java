package app.service;

import app.dao.BusquedaDAO;
import app.model.Busqueda;
import app.webscraping.pages.GoogleMainPage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BusquedaServiceTest {

  @Autowired
  private BusquedaService busquedaService;

  @MockBean
  private BusquedaDAO busquedaDAO;

  @MockBean
  private GoogleMainPage googleMainPage;

  @Test
  public void updateBusquedas_whenCalled_shouldExecuteMethodsToUpdateSearchs()
    throws Exception {
    List<Busqueda> dummySpySearches = List.of(
      spy(new Busqueda(1, "otro1", 100L)),
      spy(new Busqueda(2, "otro2", 200L)),
      spy(new Busqueda(2, "otro3", 300L))
    );
    Map<String, Long> dummySearchesResults = Map.of(
      "otro1",
      100_000L,
      "otro2",
      200_000L,
      "otro3",
      300_000L
    );
    when(busquedaDAO.getBusquedas()).thenReturn(dummySpySearches);
    when(busquedaDAO.getBusquedas2()).thenReturn(dummySpySearches);
    when(
      googleMainPage.numberOfResultsOfSearches(
        List.of("otro1", "otro2", "otro3")
      )
    )
      .thenReturn(dummySearchesResults);

    busquedaService.updateBusquedas();

    verify(dummySpySearches.get(0)).setNum_resultados(100_000L);
    verify(dummySpySearches.get(1)).setNum_resultados(200_000L);
    verify(dummySpySearches.get(2)).setNum_resultados(300_000L);

    verify(busquedaDAO).updateBusquedasResults(dummySpySearches);
  }
}
