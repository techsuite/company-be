package app.service;

import app.dao.EmpleadoDAO;
import app.model.Empleado;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.SQLException;

import static app.model.Empleado.Categoria;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmpleadoServiceTest {

  @Autowired
  private EmpleadoService empleadoService;

  @MockBean
  private EmpleadoDAO empleadoDAO;

  @Test
  public void exists_whenCalledAndThereIsNoEmpleado_shouldReturnFalse()
    throws Exception {
    when(empleadoDAO.getEmpleado(anyString())).thenReturn(null);
    when(empleadoDAO.getEmpleado2(anyString())).thenReturn(null);

    assertThat(empleadoService.exists("z17m051")).isFalse();
  }

  @Test
  public void exists_whenCalledAndItActuallyExists_shouldReturnTrue()
    throws Exception {
    Empleado mockEmpleado = new Empleado(
      "z17m051",
      "Julio Castro",
      null,
      Categoria.executives
    );
    when(empleadoDAO.getEmpleado(anyString())).thenReturn(mockEmpleado);
    when(empleadoDAO.getEmpleado2(anyString())).thenReturn(mockEmpleado);

    assertThat(empleadoService.exists("z17m051")).isTrue();
  }

  @Test
  public void exists_whenCalledAndAnExceptionIsThrown_shouldReturnFalse()
    throws Exception {
    when(empleadoDAO.getEmpleado(anyString())).thenThrow(SQLException.class);
    when(empleadoDAO.getEmpleado2(anyString())).thenThrow(SQLException.class);

    assertThat(empleadoService.exists("z17m051")).isFalse();
  }
}
