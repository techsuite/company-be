package app.dao;

import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Connection;
import java.sql.SQLException;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TecnologiaDaoTest extends GenericDaoTest {

  @Autowired
  private TecnologiaDAO tecnologiaDAO;

  @Test
  public void getTecnologias_whenCalledShouldExecuteQueryRunnerQueryMethod()
    throws Exception {
    tecnologiaDAO.getTecnologias();

    verify(queryRunner)
      .query(eq(connection), anyString(), any(BeanListHandler.class));
  }

  @Test(expected = SQLException.class)
  public void getTecnologias_whenAnExceptionHappens_shouldThrowIt()
    throws Exception {
    when(queryRunner.query(any(Connection.class), anyString(), any()))
      .thenThrow(SQLException.class);

    tecnologiaDAO.getTecnologias();
  }
}
