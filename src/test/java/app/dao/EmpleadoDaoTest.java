package app.dao;

import static app.model.Empleado.Categoria;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

import app.model.Empleado;
import java.sql.SQLException;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmpleadoDaoTest extends GenericDaoTest {

  @Autowired
  private EmpleadoDAO empleadoDAO;

  @Test
  public void getEmpleado_whenCalled_shouldSetCorrectParams() throws Exception {
    when(resultSet.next()).thenReturn(true);
    when(resultSet.getString("categoria")).thenReturn("developers");

    empleadoDAO.getEmpleado("dummy");

    verify(preparedStatement).setString(1, "dummy");
  }

  @Test(expected = SQLException.class)
  public void getEmpleado_whenCalledAndAnExceptionIsThrownInPrepareStatement_shouldThrowIt()
    throws Exception {
    when(connection.prepareStatement(anyString()))
      .thenThrow(SQLException.class);

    empleadoDAO.getEmpleado("dummy");
  }

  @Test
  public void getEmpleado_whenCalledAndThereIsNoResult_shouldReturnNull()
    throws Exception {
    when(resultSet.next()).thenReturn(false);

    Empleado ouput = empleadoDAO.getEmpleado("dummy");

    assertThat(ouput).isNull();
  }

  @Test
  public void getEmpleados_whenCalled_shouldExecuteRunnerQuery()
    throws Exception {
    empleadoDAO.getEmpleados();

    verify(queryRunner)
      .query(eq(connection), anyString(), any(BeanListHandler.class));
  }

  @Test
  public void getEmpleado2_whenCalled_shouldExecuteRunnerQuery()
    throws Exception {
    empleadoDAO.getEmpleado2("dummyId");

    verify(queryRunner)
      .query(
        eq(connection),
        anyString(),
        any(BeanHandler.class),
        eq("dummyId")
      );
  }

  @Test(expected = SQLException.class)
  public void getEmpleado2_whenCalledAndAnExceptionIsThrown_shouldThrowIt()
    throws Exception {
    when(queryRunner.query(any(), anyString(), any(), anyString()))
      .thenThrow(SQLException.class);

    empleadoDAO.getEmpleado2("dummy");
  }

  @Test
  public void insertEmpleado_whenCalled_shouldExecuteRunnerInsert()
    throws Exception {
    empleadoDAO.insertEmpleado(
      new Empleado("z17m051", "Julio Castro", null, Categoria.developers)
    );

    verify(queryRunner)
      .insert(
        eq(connection),
        anyString(),
        any(ScalarHandler.class),
        eq("z17m051"),
        eq("Julio Castro"),
        eq(String.valueOf(Categoria.developers))
      );
  }
}
