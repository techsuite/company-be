package app.webscraping.pages;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GoogleMainPageTest extends GenericWebscrapingPageTest {

  @Autowired
  private GoogleMainPage googleMainPage;

  @Test
  public void numberOfResultsOfSearches_whenCalledWithSingleSearch_shouldReturnCorrectNumberOfResults() {
    when(webElement.getText())
      .thenReturn("El resultado es 123.456 y eso es todo");

    Map<String, Long> output = googleMainPage.numberOfResultsOfSearches(
      List.of("dummy")
    );

    assertThat(output.get("dummy")).isEqualTo(123_456L);
  }

  @Test
  public void numberOfResultsOfSearches_whenCalledWithMultipleSearches_shouldReturnCorrectNumberOfResults() {
    when(webDriver.findElement(By.tagName("iframe")))
      .thenReturn(webElement)
      .thenThrow(NoSuchElementException.class)
      .thenThrow(NoSuchElementException.class);
    when(webElement.getText())
      .thenReturn("Respuesta: 123.456")
      .thenReturn("El resultado puede que sea 20, pero no estoy seguro")
      .thenReturn("100.000.000.123");

    Map<String, Long> output = googleMainPage.numberOfResultsOfSearches(
      List.of("primero", "segundo", "tercero")
    );

    assertThat(output.get("primero")).isEqualTo(123_456L);
    assertThat(output.get("segundo")).isEqualTo(20L);
    assertThat(output.get("tercero")).isEqualTo(100_000_000_123L);
  }
}
