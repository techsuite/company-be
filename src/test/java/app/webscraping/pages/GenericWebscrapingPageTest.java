package app.webscraping.pages;

import app.factory.WebDriverFactory;
import org.junit.Before;
import org.mockito.Mock;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class GenericWebscrapingPageTest {

  @MockBean
  protected WebDriverFactory webDriverFactory;

  @Mock
  protected WebDriver webDriver;

  @Mock
  protected WebElement webElement;

  @Mock
  protected WebDriver.TargetLocator targetLocator;

  @Before
  public void setup() {
    when(webDriverFactory.newChromeDriver()).thenReturn(webDriver);
    when(webDriver.findElement(any())).thenReturn(webElement);
    when(webDriver.switchTo()).thenReturn(targetLocator);
  }
}
