package mocks;

import app.model.request.cuestionario.CuestionarioRequest;
import app.model.request.cuestionario.section.CuestionarioRequestCargo;
import app.model.request.cuestionario.section.CuestionarioRequestTecnologia;

import java.util.List;

import static app.model.request.cuestionario.section.CuestionarioRequestCargo.Preferencia.No;
import static app.model.request.cuestionario.section.CuestionarioRequestCargo.Preferencia.Puede;
import static app.model.request.cuestionario.section.CuestionarioRequestTecnologia.GradoConocimiento.*;

public class CuestionarioMocks {

  public static CuestionarioRequest filledRequest() {
    return new CuestionarioRequest(
      "Juan Lopez",
      "z17m051",
      List.of(
        new CuestionarioRequestTecnologia(1, Experto),
        new CuestionarioRequestTecnologia(2, Medio),
        new CuestionarioRequestTecnologia(3, Nulo)
      ),
      null,
      List.of(),
      "No tengo ninguna de las anteriores",
      List.of(
        new CuestionarioRequestCargo(10, Puede),
        new CuestionarioRequestCargo(20, No)
      ),
      "Me encantaría trabajar de Front end"
    );
  }
}
