USE company;

CREATE TABLE Empleado(
    matricula VARCHAR(7) NOT NULL,
    nombre VARCHAR(70) NOT NULL,
    foto_url TEXT,
    categoria ENUM('executives', 'developers'),
    PRIMARY KEY(matricula)
);

CREATE TABLE Busqueda(
    id INT NOT NULL AUTO_INCREMENT,
    busqueda VARCHAR(255) NOT NULL,
    num_resultados BIGINT,
    PRIMARY KEY(id)
);

CREATE TABLE Cuestionario(
	id INT NOT NULL AUTO_INCREMENT,
	comentarios_tecnologias TEXT,
	comentarios_softskills TEXT,
	comentarios_cargos TEXT,
	empleado_id VARCHAR(7),
	FOREIGN KEY(empleado_id) REFERENCES Empleado(matricula),
	PRIMARY KEY(id)	
);

CREATE TABLE Tecnologia(
	id INT NOT NULL AUTO_INCREMENT,
	nombre VARCHAR(100),
	PRIMARY KEY(id)
);
INSERT INTO Tecnologia (nombre)
	VALUES ('Java'),('MySQL'),('CSS'),('Git'),('Javascript'),('Angular'),('Spring Boot'),('Test Unitarios'),('Swagger'),('Maven'),('Metodologías Ágiles'),('Automatización-Selenium');


CREATE TABLE Cuestionario_Tecnologia(
	cuestionario_id INT,
	tecnologia_id INT,
	grado_conocimiento ENUM("Nulo","Poco","Medio","Experto"),
	FOREIGN KEY(cuestionario_id) REFERENCES Cuestionario(id),
	FOREIGN KEY(tecnologia_id) REFERENCES Tecnologia(id),
	PRIMARY KEY(cuestionario_id,tecnologia_id)
);

CREATE TABLE Softskill(
	id INT NOT NULL AUTO_INCREMENT,
	nombre VARCHAR(100),
	PRIMARY KEY(id)
);
INSERT INTO Softskill (nombre)
	VALUES ('Liderazgo'), ('Trabajo en Equipo'), ('Aprendizaje Autónomo'), ('Confianza Personal'), ('Trabajo Bajo Presión'), ('Trabajo en Situaciones de Poca Información'), ('Gestion del Tiempo'), ('Gestion de las Emociones'), ('Orientación al Cliente'), ('Compromiso'), ('Adaptación a Cambios'), ('Negociación');

CREATE TABLE Cuestionario_Softskill(
	cuestionario_id INT,
	softskill_id INT,
	rendimiento ENUM("Mal","Regular","Bien","Excelente"),
	FOREIGN KEY(cuestionario_id) REFERENCES Cuestionario(id),
	FOREIGN KEY(softskill_id) REFERENCES Softskill(id),
	PRIMARY KEY(cuestionario_id,softskill_id)
);

CREATE TABLE Cargo(
	id INT NOT NULL AUTO_INCREMENT,
	nombre VARCHAR(50),
	PRIMARY KEY(id)
);
INSERT INTO Cargo (nombre)
	VALUES ('Front End'),('Back End'),('Liderazgo'),('Testing');	

CREATE TABLE Cuestionario_Cargo(
	cuestionario_id INT,
	cargo_id INT,
	preferencia ENUM("No","Puede","Si"),
	FOREIGN KEY(cuestionario_id) REFERENCES Cuestionario(id),
	FOREIGN KEY(cargo_id) REFERENCES Cargo(id),
	PRIMARY KEY(cuestionario_id,cargo_id)
);

