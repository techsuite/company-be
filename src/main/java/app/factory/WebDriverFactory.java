package app.factory;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.stereotype.Component;

@Component
public class WebDriverFactory {
  static {
    WebDriverManager.chromedriver().setup();
  }

  public WebDriver newChromeDriver() {
    return new ChromeDriver(
      new ChromeOptions().setHeadless(true)
    );
  }
}
