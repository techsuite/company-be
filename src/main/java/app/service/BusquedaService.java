package app.service;

import app.dao.BusquedaDAO;
import app.model.Busqueda;
import app.webscraping.pages.GoogleMainPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class BusquedaService {

  @Autowired
  private BusquedaDAO busquedaDAO;

  @Autowired
  private GoogleMainPage googleMainPage;

  // Run every hour
  @Scheduled(cron = "0 0 */1 * * *")
  public void updateBusquedas() throws Exception {
    System.out.println("time to update results");

    List<Busqueda> busquedas = busquedaDAO.getBusquedas();

    Map<String, Long> busquedasUpdatedResults = googleMainPage.numberOfResultsOfSearches(
      busquedas.stream().map(Busqueda::getBusqueda).collect(Collectors.toList())
    );

    busquedas.forEach(
      b -> b.setNum_resultados(busquedasUpdatedResults.get(b.getBusqueda()))
    );

    busquedaDAO.updateBusquedasResults(busquedas);
  }
}
