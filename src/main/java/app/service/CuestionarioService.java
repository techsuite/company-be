package app.service;

import app.dao.CuestionarioDAO;
import app.dao.EmpleadoDAO;
import app.model.Cuestionario;
import app.model.Empleado;
import app.model.request.cuestionario.CuestionarioRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static app.model.Empleado.Categoria;

@Service
public class CuestionarioService {

  @Autowired
  private EmpleadoService empleadoService;

  @Autowired
  private EmpleadoDAO empleadoDAO;

  @Autowired
  private CuestionarioDAO cuestionarioDAO;

  public void newCuestionario(CuestionarioRequest cuestionarioRequest)
    throws Exception {
    String matricula = cuestionarioRequest.getMatricula();

    if (!empleadoService.exists(matricula)) {
      empleadoDAO.insertEmpleado(
        new Empleado(
          cuestionarioRequest.getMatricula(),
          cuestionarioRequest.getNombre(),
          null,
          Categoria.developers
        )
      );
    }

    Integer cuestionarioId;
    Cuestionario cuestionario = cuestionarioDAO.getCuestionario(matricula);
    if (cuestionario == null) {
      // does not exists so we have to create it and save the
      // generated id to keep going!
      cuestionarioId =
        cuestionarioDAO.insertCuestionario(
          new Cuestionario(
            null,
            cuestionarioRequest.getComentariosTecnologias(),
            cuestionarioRequest.getComentariosSoftskills(),
            cuestionarioRequest.getComentariosCargo(),
            matricula
          )
        );
    } else {
      // corresponding Cuestionario already exists, so
      // we can save its id to keep going
      cuestionarioId = cuestionario.getId();

      // But before that we have to clear existing
      // tecnnologies, softskills and cargos info.
      cuestionarioDAO.clearExistingRelations(cuestionarioId);

      // and update comments
      cuestionario.setComentarios_tecnologias(
        cuestionarioRequest.getComentariosTecnologias()
      );
      cuestionario.setComentarios_softskills(
        cuestionarioRequest.getComentariosSoftskills()
      );
      cuestionario.setComentarios_cargos(
        cuestionarioRequest.getComentariosCargo()
      );

      cuestionarioDAO.updateCuestionario(cuestionario);
    }

    // Finally, we add technologies, softskills and cargos info
    cuestionarioDAO.insertRelatedTechnologies(
      cuestionarioId,
      cuestionarioRequest.getTecnologias()
    );
    cuestionarioDAO.insertRelatedSoftskills(
      cuestionarioId,
      cuestionarioRequest.getSoftskills()
    );
    cuestionarioDAO.insertRelatedCargos(
      cuestionarioId,
      cuestionarioRequest.getCargos()
    );
  }
}
