package app.service;

import app.dao.EmpleadoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmpleadoService {

  @Autowired
  private EmpleadoDAO empleadoDAO;

  public boolean exists(String matricula) {
    try {
      return empleadoDAO.getEmpleado(matricula) != null;
    } catch (Exception ex) {
      return false;
    }
  }
}
