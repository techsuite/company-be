package app.controller;

import app.dao.BusquedaDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ViewController {

  @Autowired
  private BusquedaDAO busquedaDAO;

  @GetMapping("/home")
  public String home(Model model) throws Exception {
    model.addAttribute("busquedas", busquedaDAO.getBusquedas());
    model.addAttribute("mensaje", "hola desde spring boot");
    return "home";
  }
}
