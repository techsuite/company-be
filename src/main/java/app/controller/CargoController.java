package app.controller;

import app.dao.CargoDAO;
import app.model.Cargo;
import app.model.request.cuestionario.section.CuestionarioRequestCargo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CargoController {

  @Autowired
  private CargoDAO cargoDAO;

  @GetMapping("/cargo")
  public ResponseEntity<List<Cargo>> getCargos() throws Exception {
    List<Cargo> cargos = cargoDAO.getCargos();

    return cargos.size() > 0
      ? ResponseEntity.ok().body(cargos)
      : ResponseEntity.noContent().build();
  }

  @GetMapping("/cargo/opciones-preferencia")
  public ResponseEntity<List<CuestionarioRequestCargo.Preferencia>> getOpcionesPreferencia() {
    return ResponseEntity
      .ok()
      .body(List.of(CuestionarioRequestCargo.Preferencia.values()));
  }
}
