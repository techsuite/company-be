package app.controller;

import app.dao.BusquedaDAO;
import app.model.Busqueda;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BusquedaController {

  @Autowired
  private BusquedaDAO busquedaDAO;

  @GetMapping("/search")
  public ResponseEntity<List<Busqueda>> getBusquedas() throws Exception {
    List<Busqueda> busquedas = busquedaDAO.getBusquedas();

    return busquedas.size() > 0
      ? ResponseEntity.ok(busquedas)
      : ResponseEntity.noContent().build();
  }
}
