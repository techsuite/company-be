package app.controller;

import app.model.response.health.HealthResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthController {

  @GetMapping("/status")
  public ResponseEntity<HealthResponse> alive() {
    System.out.println("-----------------GET /status !!!-----------------");
    return ResponseEntity.ok().body(new HealthResponse("alive v6"));
  }
}
