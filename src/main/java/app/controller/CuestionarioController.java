package app.controller;

import app.model.request.cuestionario.CuestionarioRequest;
import app.service.CuestionarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CuestionarioController {

  @Autowired
  private CuestionarioService cuestionarioService;

  @PostMapping("/cuestionario")
  public ResponseEntity<Void> nuevoCuestionario(
    @RequestBody CuestionarioRequest cuestionarioRequest
  ) throws Exception {
    cuestionarioService.newCuestionario(cuestionarioRequest);
    return ResponseEntity.ok().build();
  }
}
