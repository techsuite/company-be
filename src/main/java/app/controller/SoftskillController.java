package app.controller;

import app.dao.SoftskillDAO;
import app.model.Softskill;
import app.model.request.cuestionario.section.CuestionarioRequestSoftskill;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SoftskillController {

  @Autowired
  private SoftskillDAO softskillDAO;

  @GetMapping("/softskill")
  public ResponseEntity<List<Softskill>> getSoftskills() throws Exception {
    List<Softskill> softskills = softskillDAO.getSoftskills();

    return softskills.size() > 0
      ? ResponseEntity.ok().body(softskills)
      : ResponseEntity.noContent().build();
  }

  @GetMapping("/softskill/opciones-rendimiento")
  public ResponseEntity<List<CuestionarioRequestSoftskill.Rendimiento>> getOpcionesRendimiento() {
    return ResponseEntity
      .ok()
      .body(List.of(CuestionarioRequestSoftskill.Rendimiento.values()));
  }
}
