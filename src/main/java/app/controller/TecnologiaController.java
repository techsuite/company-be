package app.controller;

import app.dao.TecnologiaDAO;
import app.model.Tecnologia;
import app.model.request.cuestionario.section.CuestionarioRequestTecnologia;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TecnologiaController {

  @Autowired
  private TecnologiaDAO tecnologiaDAO;

  @GetMapping("/tecnologia")
  public ResponseEntity<List<Tecnologia>> getTecnologias() throws Exception {
    List<Tecnologia> tecnologias = tecnologiaDAO.getTecnologias();

    return tecnologias.size() > 0
      ? ResponseEntity.ok().body(tecnologias)
      : ResponseEntity.noContent().build();
  }

  @GetMapping("/tecnologia/opciones-conocimiento")
  public ResponseEntity<List<CuestionarioRequestTecnologia.GradoConocimiento>> getOpcionesConocimiento() {
    return ResponseEntity
      .ok()
      .body(List.of(CuestionarioRequestTecnologia.GradoConocimiento.values()));
  }
}
