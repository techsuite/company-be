package app.controller;

import app.dao.EmpleadoDAO;
import app.model.Empleado;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class EmpleadoController {

  @Autowired
  EmpleadoDAO empleadoDAO;

  @GetMapping("/team/{id}")
  public ResponseEntity<Empleado> getEmpleado(@PathVariable String id)
    throws Exception {
    Empleado empleado = empleadoDAO.getEmpleado2(id);

    return empleado != null
      ? ResponseEntity.ok().body(empleado)
      : ResponseEntity.noContent().build();
  }

  @GetMapping("/team")
  public ResponseEntity<List<Empleado>> getEmpleados()
      throws Exception {
    List<Empleado> empleados = empleadoDAO.getEmpleados();

    return empleados.size() > 0
        ? ResponseEntity.ok().body(empleados)
        : ResponseEntity.noContent().build();
  }
}
