package app.webscraping.pages;

import app.factory.WebDriverFactory;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GenericPage {

  @Autowired
  private WebDriverFactory webDriverFactory;

  protected WebDriver driver;

  protected void startSession() {
    this.driver = webDriverFactory.newChromeDriver();
  }

  protected void closeSession() {
    this.driver.quit();
  }
}
