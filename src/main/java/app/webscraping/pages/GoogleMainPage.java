package app.webscraping.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class GoogleMainPage extends GenericPage {

  public Map<String, Long> numberOfResultsOfSearches(List<String> searches) {
    Map<String, Long> result = new HashMap<>();

    startSession();

    searches.forEach(
      search -> result.put(search, numberOfResultsOfSearch(search))
    );

    closeSession();

    return result;
  }

  private long numberOfResultsOfSearch(String search) {
    goToMainPage();

    driver
      .findElement(
        By.xpath("//input[@autocapitalize and @autocomplete and @autocorrect]")
      )
      .sendKeys(search + Keys.ENTER);

    String numberOfResultsStr = driver
      .findElement(By.id("result-stats"))
      .getText()
      .chars()
      .dropWhile(c -> !Character.isDigit(c))
      .takeWhile(c -> Character.isDigit(c) || c == '.' || c == ',')
      .mapToObj(c -> String.valueOf((char) c))
      .collect(Collectors.joining())
      .replaceAll("[.,]", "");

    return Long.parseLong(numberOfResultsStr);
  }

  private void goToMainPage() {
    driver.get("http://www.google.com/");
    acceptCookies();
  }

  private void acceptCookies() {
    WebElement cookiesFrame;

    try {
      cookiesFrame = driver.findElement(By.tagName("iframe"));
    } catch (NoSuchElementException ex) {
      // cookies are already accepted, we can return with no further actions
      return;
    }

    driver.switchTo().frame(cookiesFrame);
    driver.findElement(By.id("introAgreeButton")).click();
    driver.switchTo().defaultContent();
  }
}
