FROM selenium/standalone-chrome

RUN sudo apt-get update -y
RUN sudo apt-get install default-jre -y
RUN sudo apt-get install maven -y

EXPOSE 8080

COPY . /usr/src/mymaven
WORKDIR /usr/src/mymaven
CMD ["sudo","mvn","spring-boot:run"]

